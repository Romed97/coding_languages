#include <iostream>
using namespace std;


void cifrar (string str){
int fila, col;
int prueba;
for(int i=0; str[i]; i++){  // Este for simplemente es para recorrer el string que contiene la palabra


    fila=((str[i]-'a')/5)+1;    /*con esta operacion resta el numero en ascii menos 'a' que equibale a 97 al ser el numero mas bajo que iba
                                        a aparecer es mas facil controlar el numero que quiero que aparesca lo divido entre 5 que es el num de filas y col
                                         asi los priemros 5 daria 0 y en los siguientes 5 daria 1 y si no fuera por la j con este bastaria para las filas*/

    col=((str[i]-'a')%5)+1;         /*esta otra operacion es lo mismo pero con el residuo, los multipos siguen beneficiando el resultado que buscamos.
                                        en ambas operaciones le sumo 1 al final ya que los resultados empiezan desde 0 y la tabla empieza desde 1.*/

    if(str[i]=='k'){                /*este if es para solucionar el problema de que 'i' y 'j' estan en la misma posicion  este en especifico es
                                         solo para 'k', solo cambio a la posicion que nesecito al regresarlos al numero que debrian tener */
        fila=fila-1;
        col=5 - col + 1;
    }
   else if(str[i]>='j'){              /*este if tambien es para solucionar lo de la 'i' y la 'j' lo mismo solo arregla la posicion que debrian tener*/

        if(col==1){
            col= 6;
            fila=fila - 1;
        }
        col=col - 1;
    }

    cout<<fila<<col ;                   /* al final de cada iteracion imprime los numeros que representan la fila y la columna*/

}
 }
main(){

     string palabra;                        /*ya en el main solo se pregunta la palabra a cifrar y se guarda en el string que la funcion cifrar va
                                               a usar como parametro y listo*/
    cout<<"que palabra vas a cifrar  ";
    cin>>palabra;
   string str = palabra;
   cout<<"la palabra ya cifrada es: ";
   cifrar(str);
}
