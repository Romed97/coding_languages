/*Equipo 5
Hector Alfonso Perez Gonzalez
Edson Uwaldo Segura Torres
Juan Eduardo Romo López
*/

#include <iostream>
#include <cstdlib>
#include<cstring>

using namespace std;

void vigenere();
void cifrar(string);
void cesar();

int main()
{
    int opcion;
    bool repetir = true;
    do {
        system("cls");
        string palabra; 
        
        // Texto del menú que se verá cada vez
        cout << "\n\nMenu de Opciones" << endl;
        cout << "1. Cesar" << endl;
        cout << "2. Polybios" << endl;
        cout << "3. Vigenere" << endl;
        cout << "0. SALIR" << endl;
        
        cout << "\nIngrese una opcion: ";
        cin >> opcion;
        
        switch (opcion) {
            case 1:
                cesar();//Se llama la funcion cesar 
                system("pause>nul"); // Pausa
                break;
                
            case 2:
                                       /*ya en la opcion solo se pregunta la palabra a cifrar y se guarda en el string que la funcion cifrar va
                                               a usar como parametro y listo*/
                cout<<"que palabra vas a cifrar  ";
                cin>>palabra;
                cout<<"la palabra ya cifrada es: ";
                cifrar(palabra);//Se llama la funcion Cifrar
                system("pause>nul"); // Pausa
                break;
                
            case 3:
                vigenere();//Se llama la funcion vigenere
                system("pause>nul"); // Pausa            
                break;
    
            case 0:
            	repetir = false;
            	break;
        }        
    } while (repetir);
	 
    return 0;
}

void vigenere()
{   	
    string key = "clave"; //Primero se declaran las variables que vamos a usar, por ejemplo la clave.
    string word; 
    string encryptWord = "";
    string decryptWord = "";
    int cordX,cordY;
    char tabla[27][27] = 
    {
      {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'},
      {'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A'},
      {'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B'},
      {'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C'},
      {'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D'},
      {'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E'},
      {'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F'},
      {'H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G'},
      {'I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H'},
      {'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I'},
      {'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J'},
      {'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K'},
      {'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L'},
      {'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M'},
      {'O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N'},
      {'O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N'},
      {'P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'},
      {'Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'},
      {'R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','S'},
      {'S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R'},
      {'T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S'},
      {'U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'},
      {'V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U'},
      {'W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V'},
      {'X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W'},
      {'Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X'},
      {'Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y'}
    };// Se declara la tabla en la que se basara el cifrado

    cout<<"Indique el texto a codificar: ";
    
    cin>>word;

    int sizeWord = word.length();//Una vez que ya se tiene la palabra se mide su longitud y la de la llave
    int sizeKey  = key.length();
    int keyLetter = 0;//Declaramos una funcion para saber en que letra va el codigo

    for (int i = 0; i < sizeWord; i++)//Se recorre la palabra a cifrar
    {
        for (int j = 0; j < 27; j++)//Buscamos la cordenada de x en la tabla con la coincidencia de la letra en la palabra
        {
            if(toupper(word[i]) == tabla[0][j])
            {
                cordX = j;
                break;
            } 
        }

        for (int j = 0; j < 27; j++)//Buscamos la cordenada de y en la tabla con la coincidencia de la letra en la clave
        {
            if(toupper(key[keyLetter]) == tabla[j][0])
            {
                
                cordY = j;
                if (keyLetter >= sizeKey)
                {
                    keyLetter =  0 ;
                }else
                keyLetter = keyLetter + 1;
                break;
            } 
        }
        encryptWord =  encryptWord + tabla[cordY][cordX];//Guardamos la letra resultante en esta nueva varibale 
    }

    cout<<"Tu palabra codificada es: "<<encryptWord<<endl;//Imprimimos la palabra codificada
    keyLetter = 0;
    for (int i = 0; i < sizeWord; i++)
    {
        for (int j = 0; j < 27; j++)//Para descodificar Buscamos la cordenada de y en la tabla con la coincidencia de la letra en la clave
        {
            if(toupper(key[keyLetter]) == tabla[j][0])
            {
                cordY = j;
                keyLetter = keyLetter >= sizeKey ? 0 : keyLetter + 1;
                break;
            } 
        }

        for (int j = 0; j < 27; j++)//Para descodificar Buscamos la cordenada de x en la tabla con la coincidencia de la letra en la clave para saber donde esta la fila
        {
            if(toupper(encryptWord[i]) == tabla[cordY][j])
            {
                cordX = j;
                break;
            } 
        }

        
        decryptWord =  decryptWord + tabla[0][cordX];//Ponemos la letra resultante de las coordenadas y la agregamos a la palabra desencriptada
    }

    cout<<"Tu palabra decodigificada es: "<<decryptWord<<endl;//imprimimos la palabra encriptada
}


void cifrar (string str){
int fila, col;
int prueba;
for(int i=0; str[i]; i++){  // Este for simplemente es para recorrer el string que contiene la palabra


    fila=((str[i]-'a')/5)+1;    /*con esta operacion resta el numero en ascii menos 'a' que equibale a 97 al ser el numero mas bajo que iba
                                        a aparecer es mas facil controlar el numero que quiero que aparesca lo divido entre 5 que es el num de filas y col
                                         asi los priemros 5 daria 0 y en los siguientes 5 daria 1 y si no fuera por la j con este bastaria para las filas*/

    col=((str[i]-'a')%5)+1;         /*esta otra operacion es lo mismo pero con el residuo, los multipos siguen beneficiando el resultado que buscamos.
                                        en ambas operaciones le sumo 1 al final ya que los resultados empiezan desde 0 y la tabla empieza desde 1.*/

    if(str[i]=='k'){                /*este if es para solucionar el problema de que 'i' y 'j' estan en la misma posicion  este en especifico es
                                         solo para 'k', solo cambio a la posicion que nesecito al regresarlos al numero que debrian tener */
        fila=fila-1;
        col=5 - col + 1;
    }
   else if(str[i]>='j'){              /*este if tambien es para solucionar lo de la 'i' y la 'j' lo mismo solo arregla la posicion que debrian tener*/

        if(col==1){
            col= 6;
            fila=fila - 1;
        }
        col=col - 1;
    }

    cout<<fila<<col ;                   /* al final de cada iteracion imprime los numeros que representan la fila y la columna*/

}
 }

void cesar (){
    
 int j;
 char p[100]; //se declara la variable p y su contenido maximo de 100 carcteres.
 char alf[]="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; //Se declara las variable alf y su valor.
  

  cout<<"ingrese palabra que deseas cifrar"<<endl; //se imprime en pantalla el mensaje.
  cin>>p;//se guarda la palbra en la variable p.
 
   for(int i=0;i<strlen(p);i++){ /*se utiliza strlen que es una funcion que nos indica la cantidad de letras en una palabra.
    el for se detendra hasta que i llegue al valor de letras que tiene p*/
    j=0;
    while(j<52)//mientras j sea menor que 52 que es el numero de caracteres dentro de alf, seguira entrando el while.
    {
	if(p[i]==alf[j] && (j<23||(j>=26 && j<49))) //si la letra que se busca en p son todas excepto x.y.z.X,Y,Z ingresara a la condicionante.
    {
	p[i]=alf[j+2];//se le incremente 3 valores a j para que nos de la 3 posicion despues de la letra posicionada en p.
    j=52;//se iguala j a 52 para concluir el ciclo.
    }
    if(p[i]==alf[j] && (j>23 && j<26)) //aplica para los casos especiales cuando la letra es x,y,z,
	{
        
        if(p[i]=='y')//si la posicion en p es la letra y.
	      {
        p[i]='a';//entonces se le asignara la letra a.
          }
        if(p[i]=='z')//si la posicion en p es la letra z.
	    {
        p[i]='b';//entonces se le asignara la letra b.
        }
    j=52;//se iguala j a 52 para concluir el ciclo.
    }
    if(p[i]==alf[j] && (j>49 && j<52))
    {//Si la posicion de la letra en p esta en alf y tiene el valor de X,Y o Z.
        
        if(p[i]=='Y')//si el valor en p es Y.
       	{
        p[i]='A';//entonces se le da el valor de A.
        }
        if(p[i]=='Z')//si el valor en p es Z.
	    {
        p[i]='B';//entonces se le da el valor de B.
        }
        j=52; //se iguala j a 52 para concluir el ciclo.
    }
   j++; //se incrementa el valor de J+1.
  }
 }
 cout<<p<<endl; //se imprime las letras cifradas en pantalla.
 

  cout<<"ingrese la palabra que deseas decifrar"<<endl;//se imprime el mensaje en pantalla.
  cin>>p;//la palabra se guarda en la variable p.

 	for(int i=0;i<strlen(p);i++)/*se utiliza strlen que es una funcion que nos indica la cantidad de letras en una palabra
    el for se detendra hasta que i llegue al valor de letras que tiene p*/
	{
    j=0;
      while(j<52)//mientras j sea menor que 52 que es el numero de caracteres dentro de alf, seguira entrando el while.
	   {
       if(p[i]==alf[j] && (j<23||(j>=26 &&j<49)))//si la letra que se busca en p son todas excepto x.y.z.X,Y,Z ingresara a la condicionante.
	   {
       p[i]=alf[j-2];//se le decrementa 3 valores a j para que nos de la 3 posicion antes de la letra posicionada en p.
       j=52;//se iguala j a 52 para concluir el ciclo.
       }
    if(p[i]==alf[j] && (j>23 && j<26))//aplica para los casos especiales cuando la letra es x,y,z.
	{
        
        if(p[i]=='a')//Si la letra posicionada en p es a.
	    {
        p[i]='y';//entonces se le da el valor de y.
        }
        if(p[i]=='b')//si la letra posicionada en p es b.
	    {
        p[i]='z';//entonces se le da el valor en z.
        }
    j=52;//se iguala j a 52 para concluir el ciclo.
    }
    if(p[i]==alf[j] && (j>49 && j<52)) //aplica para los casos especiales cuando la letra es x,y,z.
	{
        if(p[i]=='B')//si la letra posicioada en p es B.
       	{
        p[i]='Z'; //entonces se le da el valor en Z.
        }
        if(p[i]=='A')//si la letra posicionada en p es A.
		{
        p[i]='Y';//entonces se le da el valor de Y.
        }
    j=52;// se iguala j a 52 para concluir el ciclo.
    }
  j++;//se inclremente j+1 para salir del ciclo.
  }
}
    cout<<p<<endl; //se imprime en pantalla el valor de p.
  
      return; 
}